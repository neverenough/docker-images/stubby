#######################
#
# Configuration-stage
#
#######################
FROM jinja:latest as stage-configuration

LABEL maintainer="iugin"

ARG STUBBY_LOG_LEVEL="INFO"
# GETDNS_TRANSPORT_TLS - GETDNS_TRANSPORT_UDP - GETDNS_TRANSPORT_TCP
ARG TRANSPORT_PROTOCOL="GETDNS_TRANSPORT_TLS"
# For Strict use        GETDNS_AUTHENTICATION_REQUIRED
# For Opportunistic use GETDNS_AUTHENTICATION_NONE
ARG USAGE_PROFILE="GETDNS_AUTHENTICATION_REQUIRED"
ARG EDNS0_PAD=128
ARG EDNS0_ECS1=1
ARG EDNS0_ECS1_TIMEOUT=10000
ARG DISTRIBUTED_QUERIES=1

COPY config /app/templates
RUN mkdir -p /app/config

# Use the engine that renders the /app/templates into the /app/config
RUN /app/entrypoint.sh jinja -i /app/templates -o /app/config -c -l $STUBBY_LOG_LEVEL


#######################
#
# Stubby
#
#######################
FROM debian:buster-slim as stubby-install

LABEL maintainer="iugin"

ENV GETDNS_VERSION "v1.6.0"

WORKDIR /tmp/src

# Using pipe fails on any command failed
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Clean output only during build
ARG DEBIAN_FRONTEND=noninteractive

# Download stubby
RUN set -e -x && \
    git_deps="ca-certificates git" && \
    apt-get update && apt-get install --no-install-recommends -y $git_deps
RUN git clone https://github.com/getdnsapi/getdns.git && \
    cd getdns && \
    git checkout "${GETDNS_VERSION}" && \
    git submodule update --init

# Install dependencies
RUN set -e -x && \
    build_deps="ca-certificates gcc make cmake check libsystemd-dev build-essential libssl-dev libyaml-dev" && \
    apt-get update && apt-get install --no-install-recommends -y $build_deps

COPY --from=openssl:latest /opt/openssl /opt/openssl

# Install stubby
RUN set -e -x && \
    cd /tmp/src/getdns && \
    mkdir build && \
    cd build && \
    cmake \
        -DBUILD_STUBBY=ON \
        -DENABLE_STUB_ONLY=ON \
        -DCMAKE_INSTALL_PREFIX=/opt/stubby \
        -DOPENSSL_INCLUDE_DIR=/opt/openssl \
        -DOPENSSL_CRYPTO_LIBRARY=/opt/openssl/lib/libcrypto.so \
        -DOPENSSL_SSL_LIBRARY=/opt/openssl/lib/libssl.so \
        -DUSE_LIBIDN2=OFF \
        -DBUILD_LIBEV=OFF \
        -DBUILD_LIBEVENT2=OFF \
        -DBUILD_LIBUV=OFF .. && \
    make && \
    make install

# Clean image
RUN set -e -x && \
    apt-get purge -y --auto-remove \
        $git_deps \
        $build_deps && \
    rm -rf \
        /tmp/* \
        /var/tmp/* \
        /var/lib/apt/lists/*

# TODO
# RUN groupadd getdns && \
#     useradd -g getdns -s /etc -d /dev/null getdns && \
#     ...
#     mkdir -p /opt/getdns/var/run/ && \
#     chmod 777 /opt/getdns/var/run/

#######################
#
# Runtime image
#
#######################
FROM debian:buster-slim 

LABEL maintainer="iugin"

ENV STUBBY_INSTALLATION_DIR "/opt/stubby"
ENV STUBBY_CONFIGURATION "${STUBBY_INSTALLATION_DIR}/etc/stubby/stubby.yml"

COPY --from=openssl /opt/openssl /opt/openssl
COPY --from=stubby-install /opt/stubby ${STUBBY_INSTALLATION_DIR}

# Clean output only during build
ARG DEBIAN_FRONTEND=noninteractive

# Install runtime dependencies
RUN set -e -x && \
    apt-get update && apt-get install --no-install-recommends -y \
        ca-certificates \
#       TODO to check if required
#       dns-root-data \
#       ldnsutils \
        libyaml-0-2

# TODO
# Least privilege
RUN set -e -x && \
    groupadd -r stubby && \
    useradd --no-log-init -r -g stubby stubby

# Configuration: worksdir
RUN mkdir -m 0770 /app

# Configuration: stubby
# COPY /config/stubby.yml ${STUBBY_INSTALLATION_DIR}/etc/stubby/stubby.yml
COPY --from=stage-configuration /app/config/stubby.yml ${STUBBY_INSTALLATION_DIR}/etc/stubby/stubby.yml

# Set permissions
RUN set -e -x && \
    chown root:stubby -R \
        /app \
        ${STUBBY_INSTALLATION_DIR}/etc/stubby/stubby.yml && \
    chmod 750 -R \
        /app \
        ${STUBBY_INSTALLATION_DIR}/etc/stubby/stubby.yml

# Set capability to bind on privileged port
RUN set -e -x && \
    apt-get update && apt-get install --no-install-recommends -y \
        libcap2-bin && \
    setcap 'cap_net_bind_service=+ep' ${STUBBY_INSTALLATION_DIR}/bin/stubby

# Clean image
RUN set -e -x && \
    apt-get purge -y --auto-remove \
        libcap2-bin && \
    rm -rf \
        /tmp/* \
        /var/tmp/* \
        /var/lib/apt/lists/*

# Copy the entrypoint
COPY --chown=root:stubby entrypoint.sh /app/entrypoint.sh

WORKDIR /app

ENTRYPOINT [ "/app/entrypoint.sh" ]

USER stubby

CMD [ "stubby", "-v", "2" ]